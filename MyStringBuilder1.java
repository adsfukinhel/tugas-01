package praktikum_pemdas;
public class MyStringBuilder1 {
    private StringBuilder stringBuilder;

    public MyStringBuilder1(String s) {
        this.stringBuilder = new StringBuilder(s);
    }

    public MyStringBuilder1 append(MyStringBuilder1 s) {
        this.stringBuilder.append(s.toString());
        return this;
    }

    public MyStringBuilder1 append(int i) {
        this.stringBuilder.append(i);
        return this;
    }

    public int length() {
        return this.stringBuilder.length();
    }

    public char charAt(int index) {
        return this.stringBuilder.charAt(index);
    }

    public MyStringBuilder1 toLowerCase() {
        String lowerCaseString = this.stringBuilder.toString().toLowerCase();
        return new MyStringBuilder1(lowerCaseString);
    }

    public MyStringBuilder1 substring(int begin, int end) {
        String subString = this.stringBuilder.substring(begin, end);
        return new MyStringBuilder1(subString);
    }

    @Override
    public String toString() {
        return this.stringBuilder.toString();
    }

    public static void main(String[] args) {
        MyStringBuilder1 builder = new MyStringBuilder1("Hello");

        // Test methods
        builder.append(new MyStringBuilder1(" World"));
        builder.append(123);
        System.out.println(builder.toString()); // Output: Hello World123
        System.out.println(builder.length()); // Output: 13
        System.out.println(builder.charAt(6)); // Output: W
        System.out.println(builder.toLowerCase()); // Output: hello world123
        System.out.println(builder.substring(6, 11)); // Output: World
    }
}
