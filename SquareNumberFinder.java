package praktikum_pemdas;
public class SquareNumberFinder {
    private long max;
    private long currentNum;
    private int count;

    public SquareNumberFinder(long max) {
        this.max = max;
        this.currentNum = (long) Math.sqrt(max) + 1;
        this.count = 0;
    }

    public long[] findSquareNumbers() {
        long[] squareNumbers = new long[10];

        while (count < 10) {
            long square = currentNum * currentNum;
            if (square > max) {
                squareNumbers[count] = square;
                count++;
            }
            currentNum++;
        }

        return squareNumbers;
    }

    public static void main(String[] args) {
        long max = Long.MAX_VALUE;
        SquareNumberFinder finder = new SquareNumberFinder(max);
        long[] squares = finder.findSquareNumbers();

        System.out.println("10 square numbers greater than Long.MAX_VALUE are:");
        for (long square : squares) {
            System.out.println(square);
        }
    }
}
