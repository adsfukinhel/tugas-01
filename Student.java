package praktikum_pemdas;

public class Student { 
    private String name; 
    private String address; 
    private int age; 
    private double mathGrade; 
    private double englishGrade; 
    private double scienceGrade; 
    private double average; 

    public Student(){ 
    this.name = name;
    this.address = address;
    age = 0; 
    } 
    public Student(String n, String a, int ag){ 
    name = n; 
    address = a; 
    age = ag; 
    } 
    public void setName(String n){
    
        name = n; 
} 
public void setAddress(String a){ 
address = a; 
} 
public void setAge(int ag){ 
age = ag; 
} 
public void setMath(int math){ 
mathGrade = math; 
} 
public void setEnglish(int english){ 
englishGrade = english; 
} 
public void setScience(int science){ 
scienceGrade = science; 
} 
private double getAverage(){ 
double result = 0; 
result = (mathGrade+scienceGrade+englishGrade)/3; 
return result; 
} 
public void displayMessage(){ 
System.out.println("Siswa dengan nama "+name); 
System.out.println("beramalat di "+address); 
System.out.println("berumur "+age); 
System.out.println("mempunyai nilai rata rata "+getAverage()); 
} 

public static void main(String[] args) { 
    Student anna = new Student(); 
    anna.setName("Anna"); 
    anna.setAddress("Malang"); 
    anna.setAge(20); 
    anna.setMath(100); 
    anna.setScience(89); 
    anna.setEnglish(80); 
    anna.displayMessage(); 
    //menggunakan constructor lain 
    System.out.println("==================="); 
    Student chris = new Student("Chris", "Kediri", 21); chris.setMath(70); 
    chris.setScience(60); 
    chris.setEnglish(90); 
    chris.displayMessage();
    //siswa dengan nama anna dirubah informasi alamat dan umurnya melalui constructor 
System.out.println("==================="); 
var anna2 = new Student("anna", "Batu", 18); 
anna2.displayMessage(); 
//siswa denagan nama chris dirubah informasi alamat dan umurnya melalui method 
System.out.println("==================="); 
chris.setAddress("Surabaya"); 
chris.setAge(22); 
chris.displayMessage(); 
} 
}


    


